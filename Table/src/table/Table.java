package table;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Table {

    public static void main (String[] args) {
        PrintStream cout = null;
        try 
        {
            // PrintStream cout = System.out;
            cout = new PrintStream (new FileOutputStream ("c:\\temp\\abcd.txt"));
            cout.println("Hello from java");
            for (int i = 0; i < args.length; i++)
                cout.println ("param " + i + " = " + args [i]);
        } 
        catch (FileNotFoundException ex) 
        {
            System.out.print ("Nepovedlo se otevrit soubor, " + ex.getMessage());
        } 
        catch (Exception ex) 
        {
            System.out.print ("Neco se nepovedlo, " + ex.getMessage());
        } 
        finally 
        {
            cout.close();
        }
    }
    
}
